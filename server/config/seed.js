/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';
import Thing from '../api/thing/thing.model';
import User from '../api/user/user.model';
import Project from '../api/project/project.model';

Thing.find({}).remove()
  .then(() => {
    Thing.create({
      name: 'Development Tools',
      info: 'Integration with popular tools such as Bower, Grunt, Babel, Karma, ' +
             'Mocha, JSHint, Node Inspector, Livereload, Protractor, Jade, ' +
             'Stylus, Sass, and Less.'
    }, {
      name: 'Server and Client integration',
      info: 'Built with a powerful and fun stack: MongoDB, Express, ' +
             'AngularJS, and Node.'
    }, {
      name: 'Smart Build System',
      info: 'Build system ignores `spec` files, allowing you to keep ' +
             'tests alongside code. Automatic injection of scripts and ' +
             'styles into your index.html'
    }, {
      name: 'Modular Structure',
      info: 'Best practice client and server structures allow for more ' +
             'code reusability and maximum scalability'
    }, {
      name: 'Optimized Build',
      info: 'Build process packs up your templates as a single JavaScript ' +
             'payload, minifies your scripts/css/images, and rewrites asset ' +
             'names for caching.'
    }, {
      name: 'Deployment Ready',
      info: 'Easily deploy your app to Heroku or Openshift with the heroku ' +
             'and openshift subgenerators'
    });
  });

User.find({}).remove()
  .then(() => {
    User.create({
      provider: 'local',
      name: 'Test User',
      email: 'test@example.com',
      password: 'test'
    }, {
      provider: 'local',
      role: 'admin',
      name: 'Admin',
      email: 'admin@example.com',
      password: 'admin'
    })
    .then(() => {
      console.log('finished populating users');
    });
  });


Project.find({}).remove()
  .then(() => {
    Project.create({
      name: 'Project 1',
      color: '#5e4fa2',
      info: 'Some info about project 1',
      budgetedTime: 400,
      tasks: [{
        name: 'task number one',
        time: 5,
        budgetedTime: 10
      },{
        name: 'task number two',
        time: 0,
        budgetedTime: 10
      },{
        name: 'task number three',
        time: 42,
        budgetedTime: 60
      },{
        name: 'task number four',
        time: 12,
        budgetedTime: 16
      }],
      active: true
    },{
      name: 'Project 2',
      color: '#f46d43',
      info: 'Some info about project 2',
      budgetedTime: 20,
      tasks: [{
        name: 'task number one',
        time: 5,
        budgetedTime: 10
      },{
        name: 'task number two',
        time: 0,
        budgetedTime: 10
      },{
        name: 'task number three',
        time: 42,
        budgetedTime: 60
      },{
        name: 'task number four',
        time: 12,
        budgetedTime: 16
      }],
      active: true
    },{
      name: 'Project 3',
      color: '#66c2a5',
      info: 'Some info about project 3',
      budgetedTime: 36,
      tasks: [{
        name: 'task number one',
        time: 5,
        budgetedTime: 10
      },{
        name: 'task number two',
        time: 0,
        budgetedTime: 10
      },{
        name: 'task number three',
        time: 42,
        budgetedTime: 60
      },{
        name: 'task number four',
        time: 12,
        budgetedTime: 16
      }],
      active: true
    })
    .then(() => {
      console.log('finished populating projects');
    });
  });
