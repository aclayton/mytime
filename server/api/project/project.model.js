'use strict';

import mongoose from 'mongoose';

// var TimeSchema = new mongoose.schema({
//   user: String,
//   amount: Number
// },{
//   timestamps: true
// });

// var TaskSchema = new mongoose.Schema(
//   {
//     name: String,
//     budgetedTime: Number,
//     time: Number
//   }
// );

var ProjectSchema = new mongoose.Schema({
  name: String,
  color: String,
  info: String,
  budgetedTime: Number,
  tasks: [],
  active: Boolean
},{
  timestamps: true
});

export default mongoose.model('Project', ProjectSchema);
