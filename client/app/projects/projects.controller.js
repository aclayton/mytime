'use strict';
(function(){

class ProjectsComponent {
  constructor($scope, $http, Modal, $rootScope) {
    this.$http = $http;
    this.$scope = $scope;
    this.Modal = Modal;
    this.newProject = {};
    this.$scope.projectChartOptions = {
      percentageInnerCutout : 95,
      animationSteps : 30,
      animateRotate: true,
      animateScale: false,
      animationEasing: 'easeInSine',
      segmentShowStroke: false
    };

    $rootScope.getRandomInt = function (min, max) {
      return Math.floor(Math.random() * (max - min)) + min;
    };

    $rootScope.projectColors = ['#9e0142','#d53e4f','#f46d43','#fdae61','#fee08b','#ffffbf','#e6f598','#abdda4','#66c2a5','#3288bd','#5e4fa2'];


    this.getProjects = function () {
      this.projects = [];
      this.activeProjects = [];
      this.inactiveProjects = [];
      this.totalActiveProjects = 0;
      this.totalInactiveProjects = 0;
      this.totalBudgetedActiveProjectTime = 0;
      this.$scope.projectChartLabels = [];
      this.$scope.projectChartTime = [];
      this.$scope.projectChartColors = [];

      this.$http.get('/api/projects').then(response => {

        //variables
        this.projects = response.data;

        //cycle through all project data
        for (var i = 0; i < this.projects.length; i++){
          if (this.projects[i].active === true){

            this.totalActiveProjects = this.totalActiveProjects + 1;
            this.activeProjects.push(this.projects[i]);

          } else {

            this.totalInactiveProjects = this.totalInactiveProjects + 1;
            this.inactiveProjects.push(this.projects[i]);

          }
        }


        //cycle through active project data
        for (var j = 0; j < this.activeProjects.length; j++){

          this.totalBudgetedActiveProjectTime = this.totalBudgetedActiveProjectTime + this.activeProjects[j].budgetedTime;
          this.$scope.projectChartLabels.push(this.activeProjects[j].name);
          this.$scope.projectChartTime.push(this.activeProjects[j].budgetedTime);
          this.$scope.projectChartColors.push(this.activeProjects[j].color);
          // var target = $(this);
          // console.log(target);
          $(this).css('border-left', this.activeProjects[j].color);

        }

        console.log('projects loaded');
      });
    };


    this.deleteProject = this.Modal.confirm.delete(project => {
      this.$http.delete('/api/projects/' + project._id);
      this.getProjects();
    });



    this.openNewProjectModal = this.Modal.new.addProject(newProject => {
      this.getProjects();
    });






  }

  $onInit() {
    this.getProjects();
  }

}

angular.module('mytimeApp')
  .component('projects', {
    templateUrl: 'app/projects/projects.html',
    controller: ProjectsComponent
  });

})();
