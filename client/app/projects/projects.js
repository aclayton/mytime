'use strict';

angular.module('mytimeApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('projects', {
        url: '/projects',
        template: '<projects></projects>'
      });
  });
