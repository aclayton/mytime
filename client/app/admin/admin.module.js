'use strict';

angular.module('mytimeApp.admin', [
  'mytimeApp.auth',
  'ui.router'
]);
