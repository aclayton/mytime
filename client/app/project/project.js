'use strict';

angular.module('mytimeApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('project', {
        url: '/projects/:id',
        template: '<project></project>'
      });
  });
