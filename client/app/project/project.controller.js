'use strict';
(function(){

class ProjectComponent {
  constructor($rootScope, $scope, $http, $stateParams, Modal) {
    this.$http = $http;
    this.$scope = $scope;
    this.Modal = Modal;
    this.$stateParams = $stateParams;
    this.project = [];

    this.$scope.projectChartOptions = {
      barShowStroke: false,
      barValueSpacing: 5
    };

    $rootScope.theID = $stateParams;

    this.processTasks = function() {
      this.$scope.chartData = [[]];
      this.$scope.chartLabel = [];
      this.$scope.chartColors = [];
      // Cycle through tasks
      for (var i = 0; i < this.project.tasks.length; i++){
          this.chartTaskTime = this.project.tasks[i].budgetedTime - this.project.tasks[i].time;
          this.$scope.chartData[0].push(this.chartTaskTime);
          this.$scope.chartLabel.push('');
          if(this.chartTaskTime > 0){
            this.$scope.chartColors.push(this.project.color);
          } else {
            this.$scope.chartColors.push('#CB181D');
          }
      }
    };


    this.getProject = function() {

      this.id = this.$stateParams.id.substr(1);
      var theURL = '/api/projects/' + this.id;

      console.log('get: ' + theURL);

      this.$http.get(theURL).then(response => {
        this.project = response.data;
        this.processTasks();
      });

    };


    this.openNewTaskModal = this.Modal.new.addTask(response => {

      var theURL = '/api/projects/' + this.project._id;
      this.$http.put(theURL, this.project);
      this.processTasks();
      console.log(this.project);
      console.log('updated');

    });


  }

  $onInit() {
    this.getProject();

  }

}

angular.module('mytimeApp')
  .component('project', {
    templateUrl: 'app/project/project.html',
    controller: ProjectComponent
  });

})();
