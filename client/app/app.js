'use strict';

angular.module('mytimeApp', [
  'mytimeApp.auth',
  'mytimeApp.admin',
  'mytimeApp.constants',
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ui.router',
  'ui.bootstrap',
  'validation.match',
  'chart.js'
])
  .config(function($urlRouterProvider, $locationProvider) {

    $urlRouterProvider.otherwise('/');
    $locationProvider.html5Mode(true);

  });
