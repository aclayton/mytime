'use strict';

angular.module('mytimeApp')
  .factory('Modal', function($rootScope, $uibModal, $http) {
    /**
     * Opens a modal
     * @param  {Object} scope      - an object to be merged with modal's scope
     * @param  {String} modalClass - (optional) class(es) to be applied to the modal
     * @return {Object}            - the instance $uibModal.open() returns
     */
    function openModal(scope = {}, modalClass = 'modal-default', templateUrl = 'components/modal/modal.html') {
      var modalScope = $rootScope.$new();

      angular.extend(modalScope, scope);

      return $uibModal.open({
        templateUrl: templateUrl,
        windowClass: modalClass,
        scope: modalScope
      });
    }

    // Public API here
    return {

      new: {
        addProject(del = angular.noop){
          return function(){

            var args = Array.prototype.slice.call(arguments),
                newProject = {},
                newModal;

            newModal = openModal({
              modal:{
                newProject: newProject,
                dismissable: true,
                title: 'New Project',
                buttons: [{
                  classes: 'btn-primary',
                  text: 'Add Project',
                  click: function(e) {
                    console.log(newProject);

                    if (newProject.name && newProject.budgetedTime && newProject.info) {
                      newProject.color =  $rootScope.projectColors[$rootScope.getRandomInt(0, 10)];
                      $http.post('/api/projects', newProject);
                      newModal.close(e);
                    }

                  }
                },{
                  classes: 'btn-default',
                  text: 'Cancel',
                  click: function(e) {
                    newModal.dismiss(e);
                  }
                }]
              }
            }, 'modal-primary', 'components/modal/addProjectModal.html');

            newModal.result.then(function(event) {
              del.apply(event, args);
            });
          };
        },


        addTask(del = angular.noop){
          return function(){

            var args = Array.prototype.slice.call(arguments),
                project = args[0],
                newTask = {},
                newModal;

            newModal = openModal({
              modal:{
                newTask: newTask,
                dismissable: true,
                title: 'New Task',
                buttons: [{
                  classes: 'btn-primary',
                  text: 'Add a Task',
                  click: function(e) {

                    if (newTask.name && newTask.budgetedTime) {
                      project.tasks.push(newTask);
                      newModal.close(e);
                    }

                  }
                },{
                  classes: 'btn-default',
                  text: 'Cancel',
                  click: function(e) {
                    newModal.dismiss(e);
                  }
                }]
              }
            }, 'modal-primary', 'components/modal/addTaskModal.html');

            newModal.result.then(function(event) {
              del.apply(event, args);
            });
          };
        }



      },

      /* Confirmation modals */
      confirm: {

        /**
         * Create a function to open a delete confirmation modal (ex. ng-click='myModalFn(name, arg1, arg2...)')
         * @param  {Function} del - callback, ran when delete is confirmed
         * @return {Function}     - the function to open the modal (ex. myModalFn)
         */
        delete(del = angular.noop) {
          /**
           * Open a delete confirmation modal
           * @param  {String} name   - name or info to show on modal
           * @param  {All}           - any additional args are passed straight to del callback
           */
          return function() {
            var args = Array.prototype.slice.call(arguments),
                name = args.shift(),
                deleteModal;

            deleteModal = openModal({
              modal: {
                dismissable: true,
                title: 'Confirm Delete',
                html: '<p>Are you sure you want to delete <strong>' + name + '</strong> ?</p>',
                buttons: [{
                  classes: 'btn-danger',
                  text: 'Delete',
                  click: function(e) {
                    console.log(args);
                    deleteModal.close(e);
                  }
                }, {
                  classes: 'btn-default',
                  text: 'Cancel',
                  click: function(e) {
                    console.log(args);
                    deleteModal.dismiss(e);
                  }
                }]
              }
            }, 'modal-danger');

            deleteModal.result.then(function(event) {
              del.apply(event, args);
            });
          };
        }
      }
    };
  });
