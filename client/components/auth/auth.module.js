'use strict';

angular.module('mytimeApp.auth', [
  'mytimeApp.constants',
  'mytimeApp.util',
  'ngCookies',
  'ui.router'
])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
